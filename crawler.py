import pandas as pd
import numpy as np
from tqdm import tqdm
import time
import logging
from ast import literal_eval
from datetime import datetime, date, timedelta, timezone
import requests
import os
from dotenv import load_dotenv
from db_connector import insert_to_db

logging.basicConfig(format='%(asctime)s %(message)s', filemode='w')
logger = logging.getLogger()
logger.setLevel(logging.INFO)

load_dotenv()

RASA_X_USER = os.environ.get("RASA_X_USER", "me")
RASA_X_PASSWORD = os.environ.get("RASA_X_PASSWORD")
RASA_X_BASE_URL = os.environ.get("RASA_X_BASE_URL")

RASA_X_AUTH_URL = RASA_X_BASE_URL + "/api/auth"
RASA_X_LIST_CONV_URL = RASA_X_BASE_URL + "/api/conversations"
RASA_X_TRACKER_URL = RASA_X_BASE_URL + "/api/conversations/{}"

CRAWL_START_DATE = os.environ.get("CRAWL_START_DATE")
CRAWL_END_DATE = os.environ.get("CRAWL_END_DATE")

TIMEZONE = os.environ.get('TIMEZONE')

SCHEMA = os.environ.get("SCHEMA")
TABLE_NAME = os.environ.get('TABLE_NAME')

if SCHEMA:
    table_name = SCHEMA + "." + TABLE_NAME
else:
    table_name = TABLE_NAME


def get_timestamp(timestamp: int, format: str):
    """

    :param timestamp:
    :param format: %Y-%m-%d %H:%M:%S
    :return:
    """
    readable_timestamp = datetime.fromtimestamp(timestamp).strftime(format)

    if TIMEZONE == "global" and format == "%Y-%m-%d %H:%M:%S":
        time = datetime.strptime(readable_timestamp, format)
        hour_added = timedelta(hours=7)
        time = time + hour_added
        return str(time)

    return readable_timestamp


def login_rasa_x(username, password):
    logger.info("Login rasa")
    data = {"username": username, "password": password}
    resp = requests.post(RASA_X_AUTH_URL, json=data)
    if resp.status_code == 200:
        return resp.json()["access_token"]
    raise Exception("Wrong username or password")


def get_list_conversation_id(start_st, end_st, token):
    logger.info("Get list conv id")

    # psid is page-scope id.
    # salebot use psid as conversation_id for rasa
    headers = {"Authorization": "Bearer {}".format(token)}
    params = {"start": start_st, "until": end_st}
    resp = requests.get(RASA_X_LIST_CONV_URL, headers=headers, params=params)

    if resp.status_code == 200:
        data = resp.json()
        return [conv["sender_id"] for conv in data]
    raise Exception("Failed to get list converstation")


def get_tracker(conversation_id, token):
    logger.info("Get tracker")

    headers = {"Authorization": "Bearer {}".format(token)}
    url = RASA_X_TRACKER_URL.format(conversation_id)
    try:
        resp = requests.get(url, headers=headers)
        if resp.status_code == 200:
            data = resp.json()
            return data
    except:
        print("Failed to get tracker of converstation {}".format(conversation_id))
        return ""


def get_range_time_of_yesterday(today):
    start_dt = datetime(today.year, today.month, today.day, 0, 0, 0)
    end_dt = datetime(today.year, today.month, today.day, 23, 59, 59)
    return int(start_dt.timestamp()), int(end_dt.timestamp())


def process_raw_rasa_chatlog(data: list, date_list=None, year_list=None):
    logger.info("Process chatlog")

    fmt = "%Y-%m-%d %H:%M:%S"

    if date_list is None:
        date_list = []
    if year_list is None:
        year_list = []

    df_data = {
        "message_id": [],
        "sender_id": [],
        "sender": [],
        "user_message": [],
        "bot_message": [],
        "utter_name": [],
        "intent": [],
        "entities": [],
        "created_time": [],
        "week_day": [],
        "attachments": [],
    }

    for item in tqdm(data):
        if item == "":
            continue
        if item["events"] is not None and str(item["events"]) != "nan":
            events = item["events"]
            if not isinstance(item["events"], list):
                events = literal_eval(item["events"])
        else:
            continue
        sender_id = item["sender_id"]
        user_bot_events = [x for x in events if x["event"] == "user" or x["event"] == "bot"]

        for event_index, event in enumerate(user_bot_events):
            timestamp = get_timestamp(int(event["timestamp"]), fmt)
            # timestamp_year = get_timestamp(int(event["timestamp"]), "%Y")
            timestamp_date = get_timestamp(int(event["timestamp"]), "%Y-%m-%d")
            message_id = ""
            user_intent = ""
            week_day = datetime.strptime(timestamp_date, "%Y-%m-%d").weekday()
            # if timestamp_date in date_list or timestamp_year in year_list:
            if date_list[0] <= datetime.strptime(timestamp_date, "%Y-%m-%d") < date_list[1]:
                entity_list = ""
                if "metadata" in event:
                    if "utter_name" in event["metadata"]:
                        df_data["utter_name"].append(event["metadata"]["utter_name"])
                    else:
                        df_data["utter_name"].append("")
                else:
                    df_data["utter_name"].append("")

                if "parse_data" in event:
                    if "entities" in event["parse_data"]:
                        entities = event["parse_data"]["entities"]
                        if entities:
                            for item in entities:
                                if "value" in item:
                                    if item["value"] is not None:
                                        entity_list += str(item["value"]) + ","
                    if "intent" in event["parse_data"]:
                        if "name" in event["parse_data"]["intent"]:
                            user_intent = event['parse_data']['intent']['name']

                if "message_id" in event:
                    message_id = event["message_id"]

                message = event["text"]
                attachments = ""
                if message is None:
                    message = ""

                if "scontent" in message:
                    messsage_list = message.split("\n")
                    text_message = ""
                    for item in messsage_list:
                        if "scontent" in item:
                            attachments += item + ", "
                        else:
                            text_message += item + " "
                    message = text_message

                df_data["entities"].append(entity_list)
                df_data["sender"].append(event["event"])
                df_data["intent"].append(user_intent)
                df_data["message_id"].append(message_id)
                df_data["sender_id"].append(sender_id)
                df_data["created_time"].append(timestamp)
                df_data["week_day"].append(week_day)
                df_data["attachments"].append(attachments)

                event_owner = event["event"]
                if event_owner == "user":
                    df_data["user_message"].append(message)
                    df_data["bot_message"].append("")
                else:
                    df_data["user_message"].append("")
                    df_data["bot_message"].append(message)

    rasa_chatlog_df = pd.DataFrame.from_dict(df_data)
    return rasa_chatlog_df


def start_crawler():
    # out_dir = "/".join(CRAWL_OUTPUT_FILE_PATH.split("/")[:-1])
    # if not os.path.exists(out_dir):
    #     os.mkdir(out_dir)
    if TIMEZONE == "global":
        today = datetime.now(timezone.utc) + timedelta(hours=7)
    else:
        today = date.today()

    yesterday = today - timedelta(days=1)

    today = today.strftime("%Y-%m-%d")
    yesterday = yesterday.strftime("%Y-%m-%d")

    # start_date = yesterday
    # end_date = today
    start_date = "2020-11-16"
    end_date = "2020-11-21"

    start_time = time.time()
    token = login_rasa_x(RASA_X_USER, RASA_X_PASSWORD)
    start_st = int(datetime.strptime(start_date, "%Y-%m-%d").timestamp())
    end_st = int(datetime.strptime(end_date, "%Y-%m-%d").timestamp())
    conv_ids = get_list_conversation_id(start_st, end_st, token)
    if conv_ids:
        data = [get_tracker(conv_id, token) for conv_id in conv_ids]
        rasa_chatlog_df = process_raw_rasa_chatlog(data, [datetime.strptime(start_date, "%Y-%m-%d"), datetime.strptime(end_date, "%Y-%m-%d")], ["2020"])
        insert_to_db(rasa_chatlog_df, table_name)
    print("Run time: " + str(time.time() - start_time))


if __name__ == '__main__':
    start_crawler()
