from crawler import start_crawler
import schedule
import time
import os
from dotenv import load_dotenv

load_dotenv()

EVERYDAY_AT = os.environ.get("EVERYDAY_AT")


def main():
    schedule.every().day.at(EVERYDAY_AT).do(start_crawler)
    while True:
        schedule.run_pending()
        time.sleep(1)


if __name__ == '__main__':
    main()
